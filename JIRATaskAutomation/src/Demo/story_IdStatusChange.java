package Demo;

import org.apache.poi.ss.usermodel.Sheet;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

public class story_IdStatusChange extends all_Method{
	
    public static void main(String[] args) throws Exception 
	{
    	all_Method.initialize_PropertiesFile();
		all_Method.initialize_WebDriver();
		all_Method.login_To_Jira();
		
		//String story_Status_Sheet = obj.getProperty("excelFilePath");		
		Sheet sheet1 = workbook.getSheet("Story_ids");
		int rowCount = sheet1.getLastRowNum();
		
//        for(int i=0; i<=rowCount ; i++)
//        {         
//        	//Current State/Env - From Dev to QA
//        	String story_ID = sheet1.getRow(i).getCell(0).getStringCellValue();
//        	driver.navigate().to("https://jira.rexel.com/browse/" + story_ID);
//        	waitForElement("//li[@class='menu-item']/a/strong[contains(text(),'General')]");
//        	driver.findElement(By.xpath("//li[@class='menu-item']/a/strong[contains(text(),'General')]")).click();
//        	Thread.sleep(2000);
//        	driver.findElement(By.xpath("//strong[@title='Current State/Env']/following-sibling::div")).click();
//        	Thread.sleep(2000);
//        	changeStatus();
//        	
//        	//Testing Status: from QA not started to QA Complete
//        	waitForElement("//li[@class='menu-item']/a/strong[contains(text(),'General')]");
//        	driver.findElement(By.xpath("//li[@class='menu-item']/a/strong[contains(text(),'General')]")).click();
//        	waitForElement("//strong[@title='Testing Status']/following-sibling::div");
//        	driver.findElement(By.xpath("//strong[@title='Testing Status']/following-sibling::div")).click();
//        	waitForElement("//select[@class='select cf-select']");
//        	changeStatus();	
//        }
        
        //Write Story status in excel sheet     
		for(int i=1; i<=rowCount ; i++)
        {    
			String story_ID = sheet1.getRow(i).getCell(0).getStringCellValue();
			driver.navigate().to(obj.getProperty("jiraStoryIdURL") +story_ID);
			//driver.navigate().to("https://jira.rexel.com/browse/" + story_ID);	
//        	waitForElement("//div[@class='command-bar']");       
        	waitForElement(obj.getProperty("commandBarJiraStory"));
        	String storyStatus = driver.findElement(By.xpath(obj.getProperty("storyIdStatus"))).getText();
//			String storyStatus = driver.findElement(By.xpath("//ul[@id='opsbar-opsbar-transitions']/li[2]/a/span")).getText();			
			ExcelApiTest eat = new ExcelApiTest();
	        eat.setCellData(obj.getProperty("excelFilePath"), "Story_Final_Status",i,0, story_ID);
	        eat.setCellData(obj.getProperty("excelFilePath"), "Story_Final_Status",i,1, storyStatus);
        }   	
	}
    
	public static void changeStatus()
	{
		driver.findElement(By.xpath(obj.getProperty("generalTabStoryIdCurrentState"))).click();
		//driver.findElement(By.xpath("//select[@class='select cf-select']")).click();
    	//driver.findElement(By.xpath("//select[@class='select cf-select']")).sendKeys("QA");
    	//driver.findElement(By.xpath("//select[@class='select cf-select']")).sendKeys(Keys.ENTER);
    	//driver.findElement(By.xpath("//select[@class='select cf-select']")).sendKeys(Keys.ENTER);
		driver.findElement(By.xpath(obj.getProperty("generalTabStoryIdCurrentState"))).sendKeys("QA");
		driver.findElement(By.xpath(obj.getProperty("generalTabStoryIdCurrentState"))).sendKeys(Keys.ENTER);
		driver.findElement(By.xpath(obj.getProperty("generalTabStoryIdCurrentState"))).sendKeys(Keys.ENTER);
	}
}
