package Demo;

import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelApiTest
{
    public FileInputStream fis = null;
    public FileOutputStream fos = null;
    public XSSFWorkbook workbook = null;
//    public Sheet sheet = null;
//    public Row row = null;
//    public Cell cell = null;
 
    public void connectExcel(String xlFilePath) throws Exception
    {    
        try {
        	  //this.xlFilePath = xlFilePath;
            fis = new FileInputStream(xlFilePath);
            workbook = new XSSFWorkbook(fis);
           // fis.close();
		} catch (Exception e) {
			throw (e);
		}
    }
 
    public boolean setCellData(String xlPath, String sheetName, int rowNum, int colNumber, String value)
    {
        try
        {   
//        	System.out.println(xlPath);
//        	System.out.println(value);
        
        	connectExcel(xlPath);   	
            Sheet sheet = workbook.getSheet(sheetName);
            Row row = sheet.getRow(rowNum);
            if(row==null)
                row = sheet.createRow(rowNum);
            
            Cell cell = row.getCell(colNumber);
            if(cell == null)
                cell = row.createCell(colNumber);
 
            cell.setCellValue(value);
            fis.close();
            
            fos = new FileOutputStream("D:\\Rexel\\WorkSpace\\Feature-Sweta\\JIRATaskAutomation\\Test_Data.xlsx");
            workbook.write(fos);
            fos.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return  false;
        }
        return true;
    }
}
