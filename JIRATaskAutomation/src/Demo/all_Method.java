package Demo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.internal.Coordinates;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class all_Method {
	
	static WebDriver driver = new ChromeDriver();
	static JavascriptExecutor js = (JavascriptExecutor) driver;
	static FileInputStream finput;
	static File src;
	static XSSFWorkbook workbook;
	static Sheet sheet;
	static List<WebElement> list;
	static ArrayList<String> listoftestid = new ArrayList<String>();
	static String cycleName;
	static Properties obj = new Properties();
	
	
	
	public static void initialize_PropertiesFile() throws Exception
	{
    	FileInputStream objfile = new FileInputStream(System.getProperty("user.dir")+"\\configuration.properties");
    	obj = new Properties(System.getProperties());
    	obj.load(objfile);
	}
	
	public static void excelSheet() throws IOException
	{
		src = new File("D:\\Rexel\\WorkSpace\\Feature-Sweta\\JIRATaskAutomation\\Test_Data.xlsx");
		finput = new FileInputStream(src);
		workbook = new XSSFWorkbook(finput);
		sheet = workbook.getSheet("Credentials");
		cycleName = sheet.getRow(2).getCell(1).getStringCellValue();
	}
	
	public static void initialize_WebDriver() 
	{
		System.setProperty("webdriver.chrome.driver", "C:\\Python27\\chromedriver.exe");
		driver.manage().window().maximize();		
	}
	
	public static void login_To_Jira() throws Exception {
		// TODO Auto-generated method stub
		//initialize_WebDriver();
		driver.navigate().to("https://jira.rexel.com");
		waitForElement("//input[@id='userNameInput']");
		excelSheet();
		
		//login
		driver.findElement(By.name("UserName")).sendKeys(sheet.getRow(0).getCell(1).getStringCellValue());
		driver.findElement(By.name("Password")).sendKeys(sheet.getRow(1).getCell(1).getStringCellValue());
		driver.findElement(By.id("submitButton")).click();
		
		waitForElement("//a[@id='create_link']");  
	}
	
	public static void click_On_TestCycle_Button() throws Exception
	{
		waitForElement("//a[@id='create_link']");  
		//click on plan test cycle		
		Thread.sleep(5000);
		driver.findElement(By.id("zephyr_je.topnav.tests")).click();
		driver.findElement(By.id("zephyr-je.topnav.tests.plan.cycle")).click();
		waitForElement("//a[@id='create_link']");  	
	}
	
	public static boolean search_TestCycle() throws Exception
	{
		click_On_TestCycle_Button();
		
		//select version
		driver.findElement(By.id("select-version2-field")).clear();
		driver.findElement(By.id("select-version2-field")).sendKeys("REL 17.4");
		driver.findElement(By.id("select-version2-field")).sendKeys(Keys.ENTER);
		Thread.sleep(5000);
		excelSheet();
		boolean status = false;
		Thread.sleep(5000);
		
		List<WebElement> lst = driver.findElements(By.xpath("//ul[@id='project-panel-cycle-list-summary']/li/div/div[1]/a"));
		for(int i=0;i<lst.size();i++)
		{
			String fromJiralist = lst.get(i).getText();
			if(fromJiralist.contains(cycleName))
			{
				status=true;
				WebElement Element = driver.findElement(By.linkText(cycleName));
		        js.executeScript("arguments[0].scrollIntoView();", Element);
				break;
			}
		}
		if(status)
        {
			clickOnAddTestButton();
        }
    	else 
    	{
    		status = false; 
    		createNewTestCycle();  
    		clickOnAddTestButton();
    	}
        return status;        
	}
	
	public static void scrollIntoviewTestCycle() throws InterruptedException
	{
		List<WebElement> lst = driver.findElements(By.xpath("//ul[@id='project-panel-cycle-list-summary']/li/div/div[1]/a"));
		for(int i=0;i<lst.size();i++)
		{
			WebElement Element = driver.findElement(By.linkText(cycleName));
		    js.executeScript("arguments[0].scrollIntoView();", Element);
		    break;		
		}
	}
		
	public static void clickOnAddTestButton() throws InterruptedException, Exception
	{
		excelSheet();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		WebElement Element = driver.findElement(By.linkText(cycleName));
        js.executeScript("arguments[0].scrollIntoView();", Element);
        
        WebElement web_Element_To_Be_Hovered = driver.findElement(By.linkText(cycleName));
        Actions builder = new Actions(driver);
        builder.moveToElement(web_Element_To_Be_Hovered).build().perform();
        Thread.sleep(5000);
        
        String cycle_Settings_XPath = "//a[@title='" + cycleName + "']/following::div[1]/div/a" ;
        driver.findElement(By.xpath(cycle_Settings_XPath)).click();
        
        Thread.sleep(5000);
        List<WebElement> list = driver.findElements(By.xpath("//a[@class='aui-list-item-link cycle-operations-add-tests']"));
        int a = list.size();
        String xpath1 = "(//a[@class='aui-list-item-link cycle-operations-add-tests'])[" + a + "]";
        driver.findElement(By.xpath(xpath1)).click();
	}
	
	public static void createNewTestCycle() throws Exception
	{
		driver.findElement(By.id("pdb-create-cycle-dialog")).click();
		driver.findElement(By.id("cycle_name")).sendKeys(sheet.getRow(2).getCell(1).getStringCellValue());
		driver.findElement(By.id("cycle-create-form-submit-10820")).click(); 
		Thread.sleep(8000);
	}
		
	public static void waitForElement(String item) 
	{
	    WebDriverWait wait = new WebDriverWait(driver,30);
	    WebElement element =  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(item)));
	}
	
		
	public static void addTestToTestCycle() throws Exception
	{
		Sheet sheet1 = workbook.getSheet("Story_ids");
		int rowCount = sheet1.getLastRowNum();	
        for(int i=1; i<=rowCount ; i++)
        {         
        	String story_ID = sheet1.getRow(i).getCell(0).getStringCellValue();
        	driver.navigate().to("https://jira.rexel.com/browse/" + story_ID);
    		waitForElement("//a[@id='create_link']");  
    		WebElement Element = driver.findElement(By.xpath("//div[@class='mod-content issue-drop-zone']"));
            js.executeScript("arguments[0].scrollIntoView();", Element);
            Thread.sleep(2000);
            
            try
            {
        	   driver.findElement(By.id("show-more-links-link")).click();    
        	   Thread.sleep(2000);
            }
            
            catch(Exception e)
            {
            	//System.out.println("its a catch block");
            }
            
            list = driver.findElements(By.xpath("//*[contains(@title,'is related to')]//parent::dl//img[contains(@title,'Test')]/ancestor::p//span[1]//a"));
            for(int j=0;j<list.size();j++) 
       	    {
       	    	listoftestid.add(list.get(j).getText());
       	    }
        }
        
        search_TestCycle();
        Thread.sleep(5000);
        Iterator<String> iterator= listoftestid.iterator();
	    driver.findElement(By.id("zephyr-je-testkey-textarea")).click();
	    while (iterator.hasNext())
	    {
	    	for(int a=0; a<listoftestid.size(); a++)
	        {
	        	driver.findElement(By.id("zephyr-je-testkey-textarea")).sendKeys(listoftestid.get(a));
	        	Thread.sleep(5000);
	        	driver.findElement(By.id("zephyr-je-testkey-textarea")).sendKeys(Keys.ENTER);
	        }   			

	        Thread.sleep(3000);
	    	driver.findElement(By.xpath("//button[contains(text(),'Add')]")).click();
	    	Thread.sleep(2000);
	    	driver.findElement(By.xpath("//button[contains(text(),'Close')]")).click();
	    	break;
	    }		
	}
	
	public static void clickExecuteInTestCycle() throws Exception
	{
		excelSheet();
		List<WebElement> testIdToExecute = driver.findElements(By.xpath("//table[@id='issuetable']/tbody/tr"));
		int testIdCount = testIdToExecute.size();
		for(int j=1 ; j<=testIdCount; j++)
		{
			all_Method.scrollIntoviewTestCycle();	
			String hoverOnTestId = "//table[@id='issuetable']/tbody/tr" + "[" + j +"]";	  		    
		    WebElement Element1 = driver.findElement(By.xpath(hoverOnTestId));
		    js.executeScript("arguments[0].scrollIntoView();", Element1);
		    
			WebElement hoverOn = driver.findElement(By.xpath(hoverOnTestId));
	        Actions builder = new Actions(driver);
	        builder.moveToElement(hoverOn).build().perform();

		    String executionStatus = driver.findElement(By.xpath("//tbody/tr[" +j + "]/td[2]/div/div/div/dl/dd")).getText().toString();
		    Thread.sleep(2000);
		    if(executionStatus.equals("UNEXECUTED"))
		    {
		    	System.out.println("status is UNEXECUTED");
		    	//click on execution button
				String executeButtonId = "//tbody/tr" + "[" +j + "]" +"[@class='schedule-wrapper-tr']/td[9]/a[1]";
				driver.findElement(By.xpath(executeButtonId)).click();
				Thread.sleep(2000);
				
				//execute test case
				WebElement Element = driver.findElement(By.id("teststepDetails"));
			    js.executeScript("arguments[0].scrollIntoView();", Element);			
			    List<WebElement> allList = driver.findElements(By.xpath("//table[@class='aui mod-content-table']/tbody/tr"));
			    Thread.sleep(2000);		    
			    
			    	for(int i=1; i<=allList.size(); i++)
					{				
						String executionRow = "//table[@class='aui mod-content-table']/tbody/tr[" +i + "]/td[5]/fieldset/div/div[1]/div/dl/div";
						driver.findElement(By.xpath(executionRow)).click();
					    String executionRow2 = "//table[@class='aui mod-content-table']/tbody/tr[" +i + "]/td[5]/fieldset/div/div[2]/select";
						driver.findElement(By.xpath(executionRow2)).click();			    
						driver.findElement(By.xpath(executionRow2)).sendKeys("PASS");
					    driver.findElement(By.xpath(executionRow2)).sendKeys(Keys.ENTER);
					    Thread.sleep(2000);
					    driver.findElement(By.xpath(executionRow2)).sendKeys(Keys.TAB);		
					    Thread.sleep(2000);
					}
			    	all_Method.waitForElement("//div[@id='schedule-confirmation-dialog']");
					Thread.sleep(2000);
					driver.findElement(By.xpath("//input[@value='Execute']")).click();		    
				    Thread.sleep(2000);
				    
					//Navigate back to Test cycle page
					JavascriptExecutor js = (JavascriptExecutor) driver; 
					js.executeScript("window.history.go(-1)");
					Thread.sleep(5000);
					all_Method.scrollIntoviewTestCycle();
					
					String cycleNameXpath = "//div[@title='Click to view test execution in " + cycleName + "']";
					System.out.println("clicked on test cycle after returning");
					driver.findElement(By.xpath(cycleNameXpath)).click();
					Thread.sleep(2000);
					
					if(isAttributePresent())
					{
						System.out.println("attribute value is display: none; ");
						sheet = workbook.getSheet("Credentials");					
						String cycleNameXpath1 = "//div[@title='Click to view test execution in " + cycleName + "']";
						driver.findElement(By.xpath(cycleNameXpath1)).click();
						Thread.sleep(2000);
					}
					else System.out.println("attribute value is display: block; ");
		    }
		
		}
	}
	
	public static void executeAllTestCasePresentInCycle() throws Exception
	{
		//select version
		driver.findElement(By.id("select-version2-field")).clear();
		driver.findElement(By.id("select-version2-field")).sendKeys("REL 17.4");
		driver.findElement(By.id("select-version2-field")).sendKeys(Keys.ENTER);
		Thread.sleep(5000);		
		excelSheet();
		Thread.sleep(3000);
		all_Method.scrollIntoviewTestCycle();	
		
		String cycleNameXpath = "//div[@title='Click to view test execution in " + cycleName + "']";
		driver.findElement(By.xpath(cycleNameXpath)).click();
		Thread.sleep(2000);
		
		clickExecuteInTestCycle();
		while (isNextElementPresent())
			{
				waitForElement("//a[@id='getMoreSchedules']");
				WebElement nextLink = driver.findElement(By.id("getMoreSchedules"));
			    js.executeScript("arguments[0].scrollIntoView();", nextLink);	
			    driver.findElement(By.id("getMoreSchedules")).click();	
			    Thread.sleep(5000);
			    clickExecuteInTestCycle();
			}
	}
	
     static boolean isNextElementPresent()	
	{
    	 boolean status=false;
		try {
			
			String atrValue = driver.findElement(By.id("getMoreSchedules")).getAttribute("href");
			if(!atrValue.equals(null))
			{
				status =  true;
			}
		} 
		catch (Exception e) 
		{
			status =  false;
		}	
		return status;
	}
     
     static boolean isAttributePresent()	
 	{
     	 boolean status=false;
 		try {
 			excelSheet();
 			String xpathCyle = "//div[@title='Click to view test execution in " +cycleName +"']/following-sibling::div[contains(@style,'display: none;')]";
 			driver.findElement(By.xpath(xpathCyle));
 			status =  true;
 		} 
 		catch (Exception e) 
 		{
 			status =  false;
 		}	
 		System.out.println("Closed or Not status:" +status);
 		return status;
 	}
	 
}
	
