package Demo;

import java.util.Iterator;

import org.apache.poi.ss.usermodel.Sheet;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

public class testCaseStatusChange extends all_Method {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		
		all_Method.initialize_WebDriver();
		all_Method.login_To_Jira();
		
		Sheet sheet1 = workbook.getSheet("Story_ids");
		int rowCount = sheet1.getLastRowNum();	
		
        for(int i=0; i<=rowCount ; i++)
        {         
        	String story_ID = sheet1.getRow(i).getCell(0).getStringCellValue();
        	driver.navigate().to("https://jira.rexel.com/browse/" + story_ID);
    		waitForElement("//a[@id='create_link']");  
    		WebElement Element = driver.findElement(By.xpath("//div[@class='mod-content issue-drop-zone']"));
            js.executeScript("arguments[0].scrollIntoView();", Element);
            Thread.sleep(2000);
            try
            {
        	   driver.findElement(By.id("show-more-links-link")).click();    
        	   Thread.sleep(2000);
            }
            catch(Exception e)
            {
            	//System.out.println("its a catch block");
            }
            list = driver.findElements(By.xpath("//*[contains(@title,'is related to')]//parent::dl//img[contains(@title,'Test')]/ancestor::p//span[1]//a"));
            for(int j=0;j<list.size();j++) 
       	    {
       	    	listoftestid.add(list.get(j).getText());
       	    }
        }
        
        Iterator<String> iterator= listoftestid.iterator();
	    while (iterator.hasNext())
	    {
	    	for(int a=0; a<listoftestid.size(); a++)
	        {
	    		String testID_URL = "https://jira.rexel.com/browse/" + listoftestid.get(a);
	        	driver.navigate().to(testID_URL);
	        	waitForElement("//span[@class='trigger-label'][contains(text(),'Resolved')]");
	        	driver.findElement(By.xpath("//span[@class='trigger-label'][contains(text(),'Resolved')]")).click();
	        	waitForElement("//div[@id='workflow-transition-11-dialog']");
	        	driver.findElement(By.xpath("//input[@value='Resolved']")).click();
	        	waitForElement("//*[contains(text(),'Pass')]");
	        	driver.findElement(By.xpath("//*[contains(text(),'Pass')]")).click();
	        	waitForElement("//input[@value='Pass']");
	        	driver.findElement(By.xpath("//input[@value='Pass']")).click();
	        	Thread.sleep(3000);
	        }          
	    	break;
	    }	
	}

}
