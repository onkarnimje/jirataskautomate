package Demo;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class executeTest extends all_Method {

	public static void main(String[] args) throws Exception {
		
		all_Method.initialize_WebDriver();
		all_Method.login_To_Jira();
		all_Method.click_On_TestCycle_Button();
		all_Method.executeAllTestCasePresentInCycle();
		
	}

}
